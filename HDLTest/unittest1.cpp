#include "stdafx.h"
#include <stdlib.h>
#include "..\HDL\HDL.h"
#include "..\HDL\HDLParser.h"
#include <iostream>
#include <algorithm>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace HDL;
using namespace HDLParser;

namespace HDLTest {		
	TEST_CLASS(NANDBuildTEST) {
	public:
		TEST_METHOD(tick) {
			NANDBuild* build = new NANDBuild();
			vector<int> input(1);
			input.at(0) = 0;
			build->busmap["a"]->setValue(input);
			input.at(0) = 0;
			build->busmap["b"]->setValue(input);
			build->run(false);
			Assert::AreEqual(1, build->busmap["out"]->getValue(false)[0]);
			cout << "0 NAND 0 EQUALS 1" << endl;
			input.at(0) = 1;
			build->busmap["a"]->setValue(input);
			input.at(0) = 0;
			build->busmap["b"]->setValue(input);
			build->run(false);
			Assert::AreEqual(1, build->busmap["out"]->getValue(false)[0]);
			cout << "1 NAND 0 EQUALS 1" << endl;
			input.at(0) = 0;
			build->busmap["a"]->setValue(input);
			input.at(0) = 1;
			build->busmap["b"]->setValue(input);
			build->run(false);
			Assert::AreEqual(1, build->busmap["out"]->getValue(false)[0]);
			cout << "0 NAND 1 EQUALS 1" << endl;
			input.at(0) = 1;
			build->busmap["a"]->setValue(input);
			input.at(0) = 1;
			build->busmap["b"]->setValue(input);
			build->run(false);
			Assert::AreEqual(0, build->busmap["out"]->getValue(false)[0]);
			cout << "1 NAND 1 EQUALS 0" << endl;
		}
		TEST_METHOD(tock) {
			NANDBuild* build = new NANDBuild();
			vector<int> input(1);
			input.at(0) = 0;
			build->busmap["a"]->setValue(input);
			input.at(0) = 0;
			build->busmap["b"]->setValue(input);
			build->run(true);
			Assert::AreEqual(1, build->busmap["out"]->getValue(false)[0]);
			cout << "0 NAND 0 EQUALS 1" << endl;
			input.at(0) = 1;
			build->busmap["a"]->setValue(input);
			input.at(0) = 0;
			build->busmap["b"]->setValue(input);
			build->run(true);
			Assert::AreEqual(1, build->busmap["out"]->getValue(false)[0]);
			cout << "1 NAND 0 EQUALS 1" << endl;
			input.at(0) = 0;
			build->busmap["a"]->setValue(input);
			input.at(0) = 1;
			build->busmap["b"]->setValue(input);
			build->run(true);
			Assert::AreEqual(1, build->busmap["out"]->getValue(false)[0]);
			cout << "0 NAND 1 EQUALS 1" << endl;
			input.at(0) = 1;
			build->busmap["a"]->setValue(input);
			input.at(0) = 1;
			build->busmap["b"]->setValue(input);
			build->run(true);
			Assert::AreEqual(0, build->busmap["out"]->getValue(false)[0]);
			cout << "1 NAND 1 EQUALS 0" << endl;
		}
	};
	TEST_CLASS(BITTEST) {
		void updatebits(vector<Bit*>* bits) {
			for (int i = 9; i >= 0; i--) {
				bits->at(i)->update();
			}
		}
		TEST_METHOD(update) {
			vector<Bit*>* bits = new vector<Bit*>();
			for (int i = 0; i < 10; i++) {
				bits->push_back(new Bit());
				if (i > 0) {
					bits->back()->source = bits->at(i - 1);
				}
			}
			bits->front()->value = 1;
			vector<int> checkbits = { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 1, 1, 1, 0, 0, 0, 0, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 1, 1, 1, 1, 0, 0, 0, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 1, 1, 1, 1, 1, 0, 0, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			bits->front()->value = 0;
			checkbits = { 0, 1, 1, 1, 1, 1, 0, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 1, 1, 1, 1, 1, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 0, 0, 1, 1, 1, 1, 1 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}

			updatebits(bits);
			checkbits = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(checkbits[i], bits->at(i)->value);
			}
		}
	};
	TEST_CLASS(BUSTEST) {
		TEST_METHOD(unclocked) {
			vector<Bus*> buses = vector<Bus*>();
			for (int i = 0; i < 10; i++) {
				buses.push_back(new Bus(8, "test", false, i));
			}
			vector<int> value = { 1, 0, 1, 0, 1, 0, 1, 0 };
			buses.front()->setValue(value);
			for (int i = 0; i < 8; i++) {
				Assert::AreEqual(value[i], buses.front()->getValue(false)[i]);
			}

			for (int i = 1; i < 10; i++) {
				buses[i - 1]->connect(buses[i], 0, 8, 0, 8);
			}

			for (int i = 1; i < 10; i++) {
				buses[i]->update(false);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(value[j], buses[i]->getValue(false)[j]);
				}
			}

			value = { 0, 0, 0, 0, 0, 0, 0, 0 };
			buses.front()->setValue(value);
			for (int i = 1; i < 10; i++) {
				buses.at(i)->update(false);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(value[j], buses[i]->getValue(false)[j]);
				}
			}

			value = { 1, 0, 1, 0, 1, 0, 1, 0 };
			buses.front()->setValue(value);
			for (int i = 1; i < 10; i++) {
				buses[i]->update(true);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(value[j], buses[i]->getValue(false)[j]);
				}
			}

			value = { 0, 0, 0, 0, 0, 0, 0, 0 };
			buses.front()->setValue(value);
			for (int i = 1; i < 10; i++) {
				buses[i]->update(true);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(value[j], buses[i]->getValue(false)[j]);
				}
			}
		}
		TEST_METHOD(clocked) {
			vector<Bus*> buses = vector<Bus*>();
			for (int i = 0; i < 10; i++) {
				buses.push_back(new Bus(8, "test", true, i));
			}
			for (int i = 0; i < 10; i++) {
				Assert::AreEqual(true, buses[i]->clocked);
			}
			vector<int> value1 = { 1, 0, 1, 0, 1, 0, 1, 0 };
			vector<int> value0 = { 0, 0, 0, 0, 0, 0, 0, 0 };
			vector<vector<int>*> values = { &value1, &value0, &value0, &value0, &value0, &value0, &value0, &value0, &value0, &value0 };
			buses.front()->setValue(value1);
			for (int i = 0; i < 8; i++) {
				Assert::AreEqual(value1[i], buses.front()->getValue(true)[i]);
			}

			for (int i = 1; i < 10; i++) {
				buses[i - 1]->connect(buses[i], 0, 8, 0, 8);
			}

			for (int i = 1; i < 10; i++) {
				Assert::AreEqual(buses[i]->bits.size(), buses[i]->outs.size());
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(buses[i]->bits[j].source == &buses[i - 1]->outs[j], true);
					Assert::AreEqual(buses[i]->outs[j].source == &buses[i]->bits[j], true); // whats wrong with this line?
				}
			}

			for (int i = 1; i < 10; i++) {
				buses[i]->update(false);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(values[i]->at(j), buses[i]->getValue(true)[j]);
				}
			}

			buses.front()->setValue(value1);
			for (int i = 1; i < 10; i++) {
				buses[i]->update(false);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(values[i]->at(j), buses[i]->getValue(true)[j]);
				}
			}

			values = { &value1, &value1, &value0, &value0, &value0, &value0, &value0, &value0, &value0, &value0 };
			for (int i = 0; i < 10; i++) {
				buses[i] = new Bus(8, "test", true, i);
			}
			for (int i = 1; i < 10; i++) {
				buses[i - 1]->connect(buses[i], 0, 8, 0, 8);
			}

			buses.front()->setValue(value1);
			for (int i = 0; i < 10; i++) {
				Logger::WriteMessage("for new");
				string newstring, newstring2;
				for (int j = 0; j < 8; j++) {
					newstring.append(to_string(buses[i]->getValue(true)[j]));
					newstring2.append(to_string(buses[i]->bits[j].value));
					//Logger::WriteMessage(to_string(buses[i]->bits[j].value).c_str());
					//Logger::WriteMessage(to_string(buses[i]->getValue(true)[j]).c_str());
				}
				Logger::WriteMessage(newstring2.c_str());
				Logger::WriteMessage(newstring.c_str());
			}
			Logger::WriteMessage("for new");
			buses[0]->update(true);
			buses[1]->update(false);
			for (int i = 1; i < 10; i++) {
				//buses[i]->update(false);
				buses[i]->update(true);
				//for (int j = 0; j < 8; j++) {
				//	Assert::AreEqual(buses[i]->bits[j].value, buses[i - 1]->outs[j].value);  // this should be working but it doesn't
				//}
			}
			for (int i = 0; i < 10; i++) {
				Logger::WriteMessage("for new");
				string newstring, newstring2;
				for (int j = 0; j < 8; j++) {
					newstring.append(to_string(buses[i]->getValue(true)[j]));
					newstring2.append(to_string(buses[i]->bits[j].value));
					//Logger::WriteMessage(to_string(buses[i]->bits[j].value).c_str());
					//Logger::WriteMessage(to_string(buses[i]->getValue(true)[j]).c_str());
				}
				Logger::WriteMessage(newstring2.c_str());
				Logger::WriteMessage(newstring.c_str());
			}
			for (int i = 1; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(values[i]->at(j), buses[i]->getValue(true)[j]);
				}
			}

			values = { &value1, &value1, &value0, &value0, &value0, &value0, &value0, &value0, &value0, &value0 };
			buses.front()->setValue(value1);
			for (int i = 1; i < 10; i++) {
				buses[i]->update(true);
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 8; j++) {
					Assert::AreEqual(values[i]->at(j), buses[i]->getValue(true)[j]);
				}
			}
		}
	};
	TEST_CLASS(IOTEST) {
		IO io = IO();
		TEST_METHOD(addBus) {
			io.addBus("a", 8, false, false);
			io.addBus("b", 2, true, false);
			io.addBus("c", 5, false, true);
			io.addBus("d", 4, true, true);
		}
		TEST_METHOD(construct) {
			io.addBus("a", 8, false, false);
			io.addBus("b", 2, true, false);
			io.addBus("c", 5, false, true);
			io.addBus("d", 4, true, true);
			unordered_map<string, Bus*> busmap = io.construct();

			//Logger::WriteMessage(to_string(busmap["a"]->clocked).c_str());
			Assert::AreEqual((size_t)8, busmap["a"]->bits.size());
			Assert::AreEqual(false, busmap["a"]->clocked);
			Assert::AreEqual(-1, *min_element(busmap["a"]->ordinance.begin(), busmap["a"]->ordinance.end()));

			Assert::AreEqual((size_t)2, busmap["b"]->bits.size());
			Assert::AreEqual(true, busmap["b"]->clocked);
			Assert::AreEqual(-1, *min_element(busmap["b"]->ordinance.begin(), busmap["b"]->ordinance.end()));

			Assert::AreEqual((size_t)5, busmap["c"]->bits.size());
			Assert::AreEqual(false, busmap["c"]->clocked);
			Assert::AreEqual(0, *min_element(busmap["c"]->ordinance.begin(), busmap["c"]->ordinance.end()));

			Assert::AreEqual((size_t)4, busmap["d"]->bits.size());
			Assert::AreEqual(true, busmap["d"]->clocked);
			Assert::AreEqual(0, *min_element(busmap["d"]->ordinance.begin(), busmap["d"]->ordinance.end()));
		}
	};
	TEST_CLASS(ARCHTEST) {
		TEST_METHOD(SimpleNandArch) {
			Arch arch = Arch();
			IO io = IO();
			io.addBus("input_a", 1, false, true);
			io.addBus("input_b", 1, false, true);
			io.addBus("output_out", 1, false, false);
			arch.addGate("Nand");
			arch.addInput("a", 0, 1, "input_a", 0, 1);
			arch.addInput("b", 0, 1, "input_b", 0, 1);
			arch.addOutput("out", 0, 1, "output_out", 0, 1);
			unordered_map<string, Bus*> mapping = io.construct();
			Logger::WriteMessage(to_string(mapping["input_a"]->ordinance.size()).c_str());
			Logger::WriteMessage(to_string(mapping["input_b"]->ordinance.size()).c_str());
			Logger::WriteMessage(to_string(mapping["output_out"]->ordinance.size()).c_str());
			Logger::WriteMessage(to_string(arch.sourceNames[0].size()).c_str());
			for (int i = 0; i < arch.sourceNames.front().size(); i++) {
				Logger::WriteMessage(arch.sourceNames.front()[i].c_str());
			}
//			Logger::WriteMessage(to_string(*min_element(begin(mapping["a"]->ordinance), end(mapping["a"]->ordinance))).c_str());
			//arch.busmap[arch.sourceNames.front()[0]]
			vector<vector<Build*>> builds = arch.construct(mapping);

			mapping["input_a"]->bits.front().value = 1;
			mapping["input_b"]->bits.front().value = 1;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(0, mapping["output_out"]->bits.front().value);

			mapping["input_a"]->bits.front().value = 1;
			mapping["input_b"]->bits.front().value = 0;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					//builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(1, mapping["output_out"]->bits.front().value);

			mapping["input_a"]->bits.front().value = 0;
			mapping["input_b"]->bits.front().value = 1;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(1, mapping["output_out"]->bits.front().value);

			mapping["input_a"]->bits.front().value = 0;
			mapping["input_b"]->bits.front().value = 0;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					//builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(1, mapping["output_out"]->bits.front().value);
		}
		TEST_METHOD(NorFromNand) {
			Arch arch = Arch();
			IO io = IO();
			io.addBus("input_a", 1, false, true);
			io.addBus("input_b", 1, false, true);
			io.addBus("pin1", 1, false, false);
			io.addBus("pin2", 1, false, false);
			io.addBus("pin3", 1, false, false);
			io.addBus("output_out", 1, false, false);
			arch.addGate("Nand");
			arch.addInput("a", 0, 1, "input_a", 0, 1);
			arch.addInput("b", 0, 1, "input_a", 0, 1);
			arch.addOutput("out", 0, 1, "pin1", 0, 1);
			arch.addGate("Nand");
			arch.addInput("a", 0, 1, "input_b", 0, 1);
			arch.addInput("b", 0, 1, "input_b", 0, 1);
			arch.addOutput("out", 0, 1, "pin2", 0, 1);
			arch.addGate("Nand");
			arch.addInput("a", 0, 1, "pin1", 0, 1);
			arch.addInput("b", 0, 1, "pin2", 0, 1);
			arch.addOutput("out", 0, 1, "pin3", 0, 1);
			arch.addGate("Nand");
			arch.addInput("a", 0, 1, "pin3", 0, 1);
			arch.addInput("b", 0, 1, "pin3", 0, 1);
			arch.addOutput("out", 0, 1, "output_out", 0, 1);

			unordered_map<string, Bus*> mapping = io.construct();
			vector<vector<Build*>> builds = arch.construct(mapping);

			mapping["input_a"]->bits.front().value = 1;
			mapping["input_b"]->bits.front().value = 1;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Logger::WriteMessage(to_string(builds.size()).c_str());
			Logger::WriteMessage(to_string(builds.front().size()).c_str());
			Logger::WriteMessage(to_string(mapping["input_a"]->bits.front().value).c_str());
			Logger::WriteMessage(to_string(mapping["input_b"]->bits.front().value).c_str());
			Logger::WriteMessage(to_string(mapping["pin1"]->bits.front().value).c_str());
			Logger::WriteMessage(to_string(mapping["pin2"]->bits.front().value).c_str());
			Logger::WriteMessage(to_string(mapping["pin3"]->bits.front().value).c_str());
			Logger::WriteMessage(to_string(mapping["output_out"]->bits.front().value).c_str());
			//Assert::AreEqual(0, mapping["output_out"]->bits.front().value);

			mapping["input_a"]->bits.front().value = 1;
			mapping["input_b"]->bits.front().value = 0;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					//builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			//Assert::AreEqual(0, mapping["output_out"]->bits.front().value);

			mapping["input_a"]->bits.front().value = 0;
			mapping["input_b"]->bits.front().value = 1;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			//Assert::AreEqual(0, mapping["output_out"]->bits.front().value);

			mapping["input_a"]->bits.front().value = 0;
			mapping["input_b"]->bits.front().value = 0;
			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					//builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			//Assert::AreEqual(1, mapping["output_out"]->bits.front().value);
		} // needs to be done in buildtest since the buses inbetween the builds are not updated by the builds themselves
		TEST_METHOD(XorFromNand) {

		} // same as above
		TEST_METHOD(DoubleNandArch) {
			Arch arch = Arch();
			IO io = IO();

			io.addBus("input_a", 2, false, true);
			io.addBus("input_b", 2, false, true);
			io.addBus("output_out", 2, false, false);
			arch.addGate("Nand");
			arch.addInput("a", 0, 1, "input_a", 0, 1);
			arch.addInput("b", 0, 1, "input_b", 0, 1);
			arch.addOutput("out", 0, 1, "output_out", 0, 1);
			arch.addGate("Nand");
			arch.addInput("a", 0, 1, "input_a", 1, 2);
			arch.addInput("b", 0, 1, "input_b", 1, 2);
			arch.addOutput("out", 0, 1, "output_out", 1, 2);

			unordered_map<string, Bus*> mapping = io.construct();
			vector<vector<Build*>> builds = arch.construct(mapping);

			mapping["input_a"]->bits.front().value = 1;
			mapping["input_a"]->bits.back().value = 1;

			mapping["input_b"]->bits.front().value = 1;
			mapping["input_b"]->bits.back().value = 1;

			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(0, mapping["output_out"]->bits.front().value);
			Assert::AreEqual(0, mapping["output_out"]->bits.back().value);

			mapping["input_a"]->bits.front().value = 1;
			mapping["input_a"]->bits.back().value = 0;

			mapping["input_b"]->bits.front().value = 1;
			mapping["input_b"]->bits.back().value = 1;

			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					//builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(0, mapping["output_out"]->bits.front().value);
			Assert::AreEqual(1, mapping["output_out"]->bits.back().value);

			mapping["input_a"]->bits.front().value = 0;
			mapping["input_a"]->bits.back().value = 1;

			mapping["input_b"]->bits.front().value = 1;
			mapping["input_b"]->bits.back().value = 1;

			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(1, mapping["output_out"]->bits.front().value);
			Assert::AreEqual(0, mapping["output_out"]->bits.back().value);

			mapping["input_a"]->bits.front().value = 0;
			mapping["input_a"]->bits.back().value = 0;

			mapping["input_b"]->bits.front().value = 1;
			mapping["input_b"]->bits.back().value = 1;

			for (int i = 0; i < builds.size(); i++) {
				for (int j = 0; j < builds[i].size(); j++) {
					builds[i][j]->run(false);
					//builds[i][j]->run(true); // not necessary
				}
			}
			mapping["output_out"]->update(false);
			Assert::AreEqual(1, mapping["output_out"]->bits.front().value);
			Assert::AreEqual(1, mapping["output_out"]->bits.back().value);
		}
	};
	TEST_CLASS(GATETEST) {
		TEST_METHOD(NorFromNand) {
			Gate gate = Gate("Nor");
			gate.io.addBus("input_a", 1, false, true);
			gate.io.addBus("input_b", 1, false, true);
			gate.io.addBus("pin1", 1, false, false);
			gate.io.addBus("pin2", 1, false, false);
			gate.io.addBus("pin3", 1, false, false);
			gate.io.addBus("output_out", 1, false, false);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "input_a", 0, 1);
			gate.arch.addInput("b", 0, 1, "input_a", 0, 1);
			gate.arch.addOutput("out", 0, 1, "pin1", 0, 1);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "input_b", 0, 1);
			gate.arch.addInput("b", 0, 1, "input_b", 0, 1);
			gate.arch.addOutput("out", 0, 1, "pin2", 0, 1);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "pin1", 0, 1);
			gate.arch.addInput("b", 0, 1, "pin2", 0, 1);
			gate.arch.addOutput("out", 0, 1, "pin3", 0, 1);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "pin3", 0, 1);
			gate.arch.addInput("b", 0, 1, "pin3", 0, 1);
			gate.arch.addOutput("out", 0, 1, "output_out", 0, 1);

			Build* build = gate.build();
			unordered_map<string, Bus*>* mapping = &build->busmap;
			vector<int> input = { 1 };
			
			input.front() = 0;
			mapping->at("input_a")->setValue(input);
			input.front() = 0;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			Assert::AreEqual(1, mapping->at("output_out")->bits.front().value);

			input.front() = 1;
			mapping->at("input_a")->setValue(input);
			input.front() = 0;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			Assert::AreEqual(0, mapping->at("output_out")->bits.front().value);

			input.front() = 0;
			mapping->at("input_a")->setValue(input);
			input.front() = 1;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			Assert::AreEqual(0, mapping->at("output_out")->bits.front().value);

			input.front() = 1;
			mapping->at("input_a")->setValue(input);
			input.front() = 1;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			Assert::AreEqual(0, mapping->at("output_out")->bits.front().value);
		}
		TEST_METHOD(NorFromNandClocked) {
			Gate gate = Gate("Nor");
			gate.io.addBus("input_a", 1, false, true);
			gate.io.addBus("input_b", 1, false, true);
			gate.io.addBus("pin1", 1, true, false);
			gate.io.addBus("pin2", 1, true, false);
			gate.io.addBus("pin3", 1, true, false);
			gate.io.addBus("output_out", 1, false, false);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "input_a", 0, 1);
			gate.arch.addInput("b", 0, 1, "input_a", 0, 1);
			gate.arch.addOutput("out", 0, 1, "pin1", 0, 1);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "input_b", 0, 1);
			gate.arch.addInput("b", 0, 1, "input_b", 0, 1);
			gate.arch.addOutput("out", 0, 1, "pin2", 0, 1);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "pin1", 0, 1);
			gate.arch.addInput("b", 0, 1, "pin2", 0, 1);
			gate.arch.addOutput("out", 0, 1, "pin3", 0, 1);
			gate.arch.addGate("Nand");
			gate.arch.addInput("a", 0, 1, "pin3", 0, 1);
			gate.arch.addInput("b", 0, 1, "pin3", 0, 1);
			gate.arch.addOutput("out", 0, 1, "output_out", 0, 1);

			Build* build = gate.build();
			unordered_map<string, Bus*>* mapping = &build->busmap;
			vector<int> input = { 1 };

			input.front() = 0;
			mapping->at("input_a")->setValue(input);
			input.front() = 0;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			build->run(true);
			Assert::AreEqual(1, mapping->at("pin1")->bits.front().value);
			Assert::AreEqual(1, mapping->at("pin2")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(0, mapping->at("pin3")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(1, mapping->at("output_out")->bits.front().value);

			input.front() = 1;
			mapping->at("input_a")->setValue(input);
			input.front() = 0;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			build->run(true);
			Assert::AreEqual(0, mapping->at("pin1")->bits.front().value);
			Assert::AreEqual(1, mapping->at("pin2")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(1, mapping->at("pin3")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(0, mapping->at("output_out")->bits.front().value);

			input.front() = 0;
			mapping->at("input_a")->setValue(input);
			input.front() = 1;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			build->run(true);
			Assert::AreEqual(1, mapping->at("pin1")->bits.front().value);
			Assert::AreEqual(0, mapping->at("pin2")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(1, mapping->at("pin3")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(0, mapping->at("output_out")->bits.front().value);

			input.front() = 1;
			mapping->at("input_a")->setValue(input);
			input.front() = 1;
			mapping->at("input_b")->setValue(input);
			build->run(false);
			build->run(true);
			Assert::AreEqual(0, mapping->at("pin1")->bits.front().value);
			Assert::AreEqual(0, mapping->at("pin2")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(1, mapping->at("pin3")->bits.front().value);
			build->run(false);
			build->run(true);
			Assert::AreEqual(0, mapping->at("output_out")->bits.front().value);
		}
	};
	TEST_CLASS(BUILDTEST) {
		// skip this no point gatetest basically proved it works.
	};
	TEST_CLASS(TESTTEST) {

	};
}

namespace ParserTest {
	TEST_CLASS(HDLTokenTEST) {
	public:
		TEST_METHOD(tokenizer) {
			string test_string = "	Nand16(a=a, b=b : p1=out);";
			Logger::WriteMessage("next line");
			Logger::WriteMessage(test_string.c_str());
			Logger::WriteMessage("tokens");
			while (test_string != "") {
				HDLToken new_token = HDLToken::nextToken(test_string);
				Logger::WriteMessage(new_token.value.c_str());
			}
			test_string = "    And(a=b[0], b=c[0] : c1=out);";
			Logger::WriteMessage("next line");
			Logger::WriteMessage(test_string.c_str());
			Logger::WriteMessage("tokens");
			while (test_string != "") {
				HDLToken new_token = HDLToken::nextToken(test_string);
				Logger::WriteMessage(new_token.value.c_str());
			}
			test_string = "	PIN p1[16], p2[16], p3[16], c1;";
			Logger::WriteMessage("next line");
			Logger::WriteMessage(test_string.c_str());
			Logger::WriteMessage("tokens");
			while (test_string != "") {
				HDLToken new_token = HDLToken::nextToken(test_string);
				Logger::WriteMessage(new_token.value.c_str());
			}
			test_string = "}";
			Logger::WriteMessage("next line");
			Logger::WriteMessage(test_string.c_str());
			Logger::WriteMessage("tokens");
			while (test_string != "") {
				HDLToken new_token = HDLToken::nextToken(test_string);
				Logger::WriteMessage(new_token.value.c_str());
			}
			test_string = "		 ";
			Logger::WriteMessage("next line");
			Logger::WriteMessage(test_string.c_str());
			Logger::WriteMessage("tokens");
			while (test_string != "") {
				HDLToken new_token = HDLToken::nextToken(test_string);
				Logger::WriteMessage(new_token.value.c_str());
			}
			test_string = "IMPORT \"mysaves/HRDRS.hdl\";";
			Logger::WriteMessage("next line");
			Logger::WriteMessage(test_string.c_str());
			Logger::WriteMessage("tokens");
			while (test_string != "") {
				HDLToken new_token = HDLToken::nextToken(test_string);
				Logger::WriteMessage(new_token.value.c_str());
			}
			test_string = "			out[0..7] = p4[8..15];";
			Logger::WriteMessage("next line");
			Logger::WriteMessage(test_string.c_str());
			Logger::WriteMessage("tokens");
			while (test_string != "") {
				HDLToken new_token = HDLToken::nextToken(test_string);
				Logger::WriteMessage(new_token.value.c_str());
			}
		}
	};
}