#pragma once

//create a namespace HDL for all the classes

#include <stdlib.h>
#include <vector>
#include <deque>
#include <unordered_map>
#include <stack>
#include <string>

//using namespace std;


namespace HDL {
	std::vector<int> toBinary(int decimal);
	std::vector<int> toVector(int binary);

	class Bus;  // generic bus, handles bitmapping between large groupings of wires, and clocking
	class IO;  // identifies and handles bus construction
	class Arch;  // identifies and handles all the component gates and bus connections between them, including splicing, for construction inside the Gate class
	class Gate;  // identifies and manages the IO and Arch class constructions into Bus forms and update orders
	class Build;  // identifies and manages the Bus forms and update order for the Test class
	class Test;  // identifies the user defined test cases and manages their running and assertion

	struct Bit {
		Bit* source = NULL;
		int value = 0;

		void update();
	};

	class Bus {
	public:
		std::string name;
		std::vector<Bit> bits;
		std::vector<Bit> outs;
		bool clocked;
		int size;
		std::vector<int> ordinance;

		Bus(int size, std::string name, bool clocked, int ordinance);
		void update(bool tock);
		void connect(Bus* other, int start, int end, int otherStart, int otherEnd);
		void setOrdinance(int start, int end, int ordinance);
		void setValue(std::vector<int>& value);
		std::vector<int> getValue(bool out);
	};

	class IO {
	public:
		std::vector<std::string> names;
		std::vector<int> sizes;
		std::vector<bool> clocks;
		std::vector<bool> inputs;

		void addBus(std::string name, int size, bool clocked, bool input);
		std::unordered_map<std::string, Bus*> construct();
	};

	class Arch {
	public:
		std::vector<std::string> gates;

		std::deque<std::vector<std::string>> inputNames;
		std::deque<std::vector<int*>> inputRanges;
		std::deque<std::vector<std::string>> sourceNames;
		std::deque<std::vector<int*>> sourceRanges;

		std::deque<std::vector<std::string>> outputNames;
		std::deque<std::vector<int*>> outputRanges;
		std::deque<std::vector<std::string>> destinationNames;
		std::deque<std::vector<int*>> destinationRanges;

		void addGate(std::string name);
		void addInput(std::string name, int start, int end, std::string source, int sourceStart, int sourceEnd);
		void addOutput(std::string name, int start, int end, std::string output, int destinationStart, int destinationEnd);
		std::vector<std::vector<Build*>> construct(std::unordered_map<std::string, Bus*> buses);

	private:
		static std::unordered_map<std::string, Gate*> allGates;
	};

	class Gate {
	public:
		IO io;
		Arch arch;
		std::string name;

		Gate() {}
		Gate(std::string name) : name(name) {}
		virtual Build* build(); //constructs a build, calls construct of IO and then Arch
	};

	class Build {
	public:
		std::string name;
		//int ordinance;// = -1;
		std::unordered_map<std::string, Bus*> busmap; //comes from IO::construct(), but taken from the same instance of the method
		std::vector<std::vector<Bus*>> buses; //created in Gate::build();
		std::vector<std::vector<Build*>> gates; //comes from Arch::construct()

		Build() {}
		Build(std::string name) : name(name) {}
		virtual void run(bool tock);
	};

	class Test {
	public:
		double time = 0.0;
		Build chip;
		std::deque<std::vector<std::string>> inputs;
		std::deque<std::vector<int>> inputValues;
		std::deque<std::vector<std::string>> outputs;
		std::deque<std::vector<int>> outputValues;
		std::deque<int> bases;

		void nextTime();
		void addBase(std::string base);
		void addInput(std::string name, int value);
		void addOutput(std::string name, int value);

		void tick(); //updates time in tick of the build
		void tock(); //updates time in tock of the build

		void run();
	};

	class NANDBuild : public Build {
	public:
		NANDBuild();
		void run(bool tock) override;
	};

	class NANDGate : public Gate {
	public:
		NANDGate() {}
		Build* build();
	};
}

//std::unordered_map<std::string, Gate*> allGates;// = { { "Nand", new NANDGate() } }; // turn into unordered_map
//std::unordered_map<std::string, Test*> allTests; // turn into unordered_map, and check deprication

/*
essential run order -
when a gate is built, it calls IO::construct(), which then returns a map. This map is passed to Arch::construct(),
which then uses it in order to create ordinance numbers for the builds within the build. Once that is complete,
a subroutine within the method, i.e. a part of the method, runs which takes the map and turns it into a vector of
vectors, in the same way the builds are stored, based on their ordinance numbers. Thus, the map is constructed,
but the vector of vectors is stored in memory.


*/
