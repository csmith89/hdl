#pragma once

#include "HDL.h"
#include <fstream>


namespace HDLParser {
	enum class Types { symbol, keyword, constant, identifier, dir };
	enum class Subtypes {
		plus, dash, comma, dotdot, open_p, close_p, open_c, close_c, open_b, close_b, colon, semicolon, vertical, equals,  // symbols
		IMPORT, CHIP, TEST, IN, OUT, PIN, CLOCK,  // keywords
		busSize, Base, Binary, Decimal,  // constants
		inName, outName, pinName, chipName,  // identifiers
		start, file, importDec, chipDec, testDec, fileDir, header, inDecs, outDecs, pinDecs, clockDecs, body, partDec, eq, in, out, busRange  // statements types, nodes with no token
	};

	//class HDLParser; the parser
	class ASTNode;  // the lexer
	class HDLToken;  // the tokenizer

	class HDLToken {
	public:
		int line_number;
		std::string value;
		Types type;
		HDLToken(std::string value, int line_number);  // creates a token given a string
		static HDLToken nextToken(std::string& stream, int line_number = -1);  // essentially the tokenizer, changes the stream in-place. Returns null if no token found, means either throw an UnrecognizedToken error or you're at the end of the line

		static bool isKeyword(std::string);
		static bool isSymbol(std::string);
		static bool isConstant(std::string);
		static bool isIdentifier(std::string);

		static std::vector<std::string> symbols;
		static std::vector<std::string> keywords;
	};

	class ASTNode {  // syntax checker, but doesn't actually parse the AST. Essentially the lexer
	public:
		HDLToken token = HDLToken("eof");
		Subtypes subtype;
		std::vector<ASTNode*> children;
		ASTNode(Subtypes subtype);
		ASTNode(HDLToken token, Subtypes subtype);
		static ASTNode* getRoot(std::vector<HDLToken> tokens, Subtypes parent_node_type);

		static std::unordered_map<std::string, Subtypes> subtypemap;
	};

	std::pair<HDL::Gate, HDL::Test> parseAST(ASTNode* head);
};

