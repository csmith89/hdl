//
// Created by Coleman on 8/20/2016.
//

#include "HDL.h"
#include <algorithm>
#include <iostream>
#include <string>

using namespace std;
using namespace HDL;

vector<int> HDL::toBinary(int decimal) {
	vector<int> returnVector;
	vector<int> reversedVector;
	while (decimal > 0) {
		reversedVector.push_back(decimal % 2);
		decimal /= 2;
	}
	return reversedVector;
}
vector<int> HDL::toVector(int binary) {
	vector<int> returnVector;
	while (binary > 0) {
		returnVector.push_back(binary % 10);
		binary /= 10;
	}
	return returnVector;
}



//unordered_map<string, Gate*> allGates = { { "Nand", new NANDGate() } };


// done
void Bit::update() {
	if (source != NULL) {
		value = source->value;
	}
}  // updates the value of the bit by copying the value of the source.


// done
Bus::Bus(int size, string name, bool clocked, int ordinance) : size(size), name(name), clocked(clocked) {
	for (int i = 0; i < size; i++) {
		bits.push_back(Bit());
		this->ordinance.push_back(ordinance);
		if (clocked) {
			outs.push_back(Bit());
		}
	}

	if (clocked) {
		for (int i = 0; i < size; i++) {
			outs[i].source = &bits[i];
		}
	}
}
void Bus::update(bool tock) {
	if (tock && clocked) {
		for (Bit& out : outs) {
			out.update();
		}
	}
	else if ((!tock && clocked) || !clocked) {
		for (Bit& bit : bits) {
			bit.update();
		}
	}
}
void Bus::connect(Bus* other, int start, int end, int otherStart, int otherEnd) {
	if (end - start != otherEnd - otherStart) {
		cout << name << " does not match " << other->name << " in range, " << start << " " << end << " " << otherStart << " " << otherEnd << endl;
		system("PAUSE");
		system("EXIT");
	}
	if (clocked) {
		for (int i = 0; i < end - start; i++) {
			other->bits[i + otherStart].source = &outs[i + start];
		}
	}
	else {
		for (int i = 0; i < end - start; i++) {
			other->bits[i + otherStart].source = &bits[i + start];
		}
	}
}  // sets "this" bus as the source of the "other" bus. From [start, end)
void Bus::setOrdinance(int start, int end, int ordinance) {
	for (int i = 0; i < end - start; i++) {
		this->ordinance[i + start] = ordinance;
	}
}
void Bus::setValue(vector<int>& value) {
	if (value.size() == bits.size()) {
		for (int i = 0; i < bits.size(); i++) {
			if (value[i] > 1) {
				cout << "You're an idiot and screwed up your bases. Goddammit" << endl;
				system("PAUSE");
				system("EXIT");
			}
			if (clocked) {
				outs[i].value = value.at(i);
			}
			bits[i].value = value.at(i);
		}
	}
}
vector<int> Bus::getValue(bool out) {
	vector<int> vec = vector<int>(bits.size());
	for (int i = 0; i < bits.size(); i++) {
		if (clocked && out) {
			vec[i] = outs[i].value;
		}
		else {
			vec[i] = bits[i].value;
		}
	}
	return vec;
}


// done
void IO::addBus(string name, int size, bool clocked, bool input) {
	names.push_back(name);
	sizes.push_back(size);
	clocks.push_back(clocked);
	inputs.push_back(input);
}
unordered_map<string, Bus*> IO::construct() {
	unordered_map<string, Bus*> buses = unordered_map<string, Bus*>();
	for (int i = 0; i < sizes.size(); i++) {
		if (inputs[i]) {
			buses.insert({ names[i], new Bus(sizes[i], names[i], clocks[i], 0) });
		}
		else {
			buses.insert({ names[i], new Bus(sizes[i], names[i], clocks[i], -1) });
		}
	}
	return buses;
}  // constructs the Bus objects designated by the IO object


// done
void Arch::addGate(string name) {
	gates.push_back(name);
	inputNames.push_back(vector<string>());
	inputRanges.push_back(vector<int*>());
	sourceNames.push_back(vector<string>());
	sourceRanges.push_back(vector<int*>());
	outputNames.push_back(vector<string>());
	outputRanges.push_back(vector<int*>());
	destinationNames.push_back(vector<string>());
	destinationRanges.push_back(vector<int*>());
}  // adds a gate to the Arch, and creates the empty input and output information used below
void Arch::addInput(string name, int start, int end, string source, int sourceStart, int sourceEnd) {
	int* arr = new int[2];
	arr[0] = start;
	arr[1] = end;
	inputNames.back().push_back(name);
	inputRanges.back().push_back(arr);

	arr = new int[2];
	arr[0] = sourceStart;
	arr[1] = sourceEnd;
	sourceNames.back().push_back(source);
	sourceRanges.back().push_back(arr);
}  // adds to the inputs of the just added gate
void Arch::addOutput(string name, int start, int end, string destination, int destinationStart, int destinationEnd) {
	int* arr = new int[2];
	arr[0] = start;
	arr[1] = end;
	outputNames.back().push_back(name);
	outputRanges.back().push_back(arr);

	arr = new int[2];
	arr[0] = destinationStart;
	arr[1] = destinationEnd;
	destinationNames.back().push_back(destination);
	destinationRanges.back().push_back(arr);
}  // adds to the outputs of the just added gate
vector<vector<Build*>> Arch::construct(unordered_map<string, Bus*> busmap) {
	deque<Build*> current_builds;
	vector<vector<Build*>> builds = vector<vector<Build*>>();
	for (string name : gates) {
		bool found = false;
		for (pair<string, Gate*> gate : allGates) {
			if (gate.first == name) {
				current_builds.push_back(allGates[name]->build());
				found = true;
			}
		}
		if (!found) {
			cout << name << " - Gate Not Found" << endl;
			system("PAUSE");
			system("EXIT");
		}
	}
	while (current_builds.size() > 0) {  // TODO add in something to prevent loops in the file, forcing some sort of ordering
		bool isConnected = true;
		int maxOrdinance = 0;
		for (int i = 0; i < sourceNames.front().size(); i++) {
			int ordinance = -1;
			ordinance = *min_element(begin(busmap[sourceNames.front()[i]]->ordinance), end(busmap[sourceNames.front()[i]]->ordinance)); // something is wrong with this line but who knows what that is something about accessing bad memory not really sure
			if (ordinance < 0) {
				isConnected = false;
				current_builds.push_back(current_builds.front());
				inputNames.push_back(inputNames.front());
				inputRanges.push_back(inputRanges.front());
				sourceNames.push_back(sourceNames.front());
				sourceRanges.push_back(sourceRanges.front());
				outputNames.push_back(outputNames.front());
				outputRanges.push_back(outputRanges.front());
				destinationNames.push_back(destinationNames.front());
				destinationRanges.push_back(destinationRanges.front());
			}
			else {
				maxOrdinance = max(maxOrdinance, ordinance);
			}
		}
		if (isConnected) {
            while (maxOrdinance >= builds.size()) {
				builds.push_back(vector<Build*>());
			}
			builds[maxOrdinance].push_back(current_builds.front());
			for (int i = 0; i < sourceNames.front().size(); i++) {
				busmap[sourceNames.front()[i]]->connect(current_builds.front()->busmap[inputNames.front()[i]], sourceRanges.front()[i][0], sourceRanges.front()[i][1], inputRanges.front()[i][0], inputRanges.front()[i][1]);
			}
			for (int i = 0; i < outputNames.front().size(); i++) {
				busmap[destinationNames.front()[i]]->setOrdinance(destinationRanges.front()[i][0], destinationRanges.front()[i][1], maxOrdinance + 1);
				current_builds.front()->busmap[outputNames.front()[i]]->connect(busmap[destinationNames.front()[i]], outputRanges.front()[i][0], outputRanges.front()[i][1], destinationRanges.front()[i][0], destinationRanges.front()[i][1]);
			}
		}
		current_builds.pop_front();
		inputNames.pop_front();
		inputRanges.pop_front();
		sourceNames.pop_front();
		sourceRanges.pop_front();
		outputNames.pop_front();
		outputRanges.pop_front();
		destinationNames.pop_front();
		destinationRanges.pop_front();
	}
	return builds;
}


// done
Build* Gate::build() {
	Build* build = new Build(name);
	build->busmap = io.construct();
	build->gates = arch.construct(build->busmap);
	for (pair<string, Bus*> bus : build->busmap) {
		int ordinance = *max_element(bus.second->ordinance.begin(), bus.second->ordinance.end());
		while (ordinance >= build->buses.size()) {
			build->buses.push_back(vector<Bus*>());
		}
		build->buses[ordinance].push_back(bus.second);
	}
	return build;
}


// done
void Build::run(bool tock) {
	int highest_ordinance = max(buses.size(), gates.size());
	for (int i = 0; i < highest_ordinance; i++) {
		if (i < buses.size()) {
			for (int j = 0; j < buses[i].size(); j++) {
				buses[i][j]->update(tock);
			}
		}
		if (i < gates.size()) {
			for (int j = 0; j < gates[i].size(); j++) {
				gates[i][j]->run(tock);
			}
		}
	}
}


// TODO needs to be done. --------------------------------------------------------DO BELOW--------------------------------------------------------------------------//
void Test::nextTime() {
	inputs.push_back(vector<string>());
	inputValues.push_back(vector<int>());
	outputs.push_back(vector<string>());
	outputValues.push_back(vector<int>());
	bases.push_back(0);
}
void Test::addBase(string base) {
	bases.back() = stoi(base);
}
void Test::addInput(string name, int value) {
	inputs.back().push_back(name);
	inputValues.back().push_back(value);
}
void Test::addOutput(string name, int value) {
	outputs.back().push_back(name);
	outputValues.back().push_back(value);
}
void Test::tick() {
    time += 0.5;
	for (int i = 0; i < inputs.front().size(); i++) {
        vector<int> set_value;
        if (bases.front() == 2) {
            set_value = toVector(inputValues.front()[i]);
        }
        else {
            set_value = toBinary(inputValues.front()[i]);
        }
        chip.busmap[inputs.front()[i]]->setValue(set_value); // should probably be fixed to remove one dimention of vectors, and to just convert the int
	}
	chip.run(false);
}
void Test::tock() {
    time += 0.5;
	chip.run(true);
	for (int i = 0; i < outputs.front().size(); i++) {
        // needs an equality checker between vectors
	}
	// now run all the checks on the outputs and cout if it fails
}
void Test::run() {
    for (int i = 0; i < bases.size(); i++) {
        tick();
        tock();
        inputs.pop_front();
        inputValues.pop_front();
        outputs.pop_front();
        outputValues.pop_front();
        bases.pop_front();
    }
}


NANDBuild::NANDBuild() : Build() {
	name = "NAND";
	buses.push_back(vector<Bus*>());
	Bus* a = new Bus(1, "a", false, 0);
	Bus* b = new Bus(1, "b", false, 0);
	Bus* out = new Bus(1, "out", false, 1);
	buses.back().push_back(a);
	buses.back().push_back(b);
	buses.back().push_back(out);
	busmap = unordered_map<string, Bus*>();
	busmap.insert({ "a", a });
	busmap.insert({ "b", b });
	busmap.insert({ "out", out });
}
void NANDBuild::run(bool tock) {
	buses.back()[0]->update(true);
	buses.back()[1]->update(true);
	if ((buses.back()[0])->bits[0].value == (buses.back()[1])->bits[0].value) {
		(buses.back())[2]->bits[0].value = 0;
		if ((buses.back()[0])->bits[0].value == 0) {
			(buses.back())[2]->bits[0].value = 1;
		}
	}
	else {
		(buses.back()[2])->bits[0].value = 1;
	}
}



Build* NANDGate::build() {
	return new NANDBuild();
}


unordered_map<string, Gate*> Arch::allGates = { { "Nand", new NANDGate() } };

//namespace BUILTIN {
//
//    Gate* Not = new Gate();
//    Gate* Nor = new Gate();
//    Gate* Nand = new Gate();
//    Gate* Or = new Gate();
//    Gate* And = new Gate();
//    Gate* Xor = new Gate();
//    Gate* Xnor = new Gate();
//
//    Gate* Not8 = new Gate();
//    Gate* Nor8 = new Gate();
//    Gate* Nand8 = new Gate();
//    Gate* Or8 = new Gate();
//    Gate* And8 = new Gate();
//    Gate* Xor8 = new Gate();
//    Gate* Xnor8 = new Gate();
//
//    Gate* Not16 = new Gate();
//    Gate* Nor16 = new Gate();
//    Gate* Nand16 = new Gate();
//    Gate* Or16 = new Gate();
//    Gate* And16 = new Gate();
//    Gate* Xor16 = new Gate();
//    Gate* Xnor16 = new Gate();
//
//
//    Gate* Mux = new Gate();
//    Gate* Demux = new Gate();
//
//    Gate* Mux8Way = new Gate();
//    Gate* Demux8Way = new Gate();
//
//    Gate* Mux16 = new Gate();
//    Gate* Demux16 = new Gate();
//
//
//    Gate* DFF = new Gate();
//    Gate* Reg = new Gate();
//
//    Gate* DFF8 = new Gate();
//    Gate* Reg8 = new Gate();
//
//    Gate* DFF16 = new Gate();
//    Gate* Reg16 = new Gate();
//
//    Gate* RAM128x8 = new Gate();
//    Gate* RAM16Kx16 = new Gate();
//
//    Gate* RAM256x8 = new Gate();
//    Gate* RAM32Kx16 = new Gate();
//
//} code in HDL as the standard libraries, up to 16 bit sizes



