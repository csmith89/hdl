//
// Created by Coleman on 8/20/2016.
//

#include "HDLParser.h"
#include <iostream>
#include <algorithm>

using namespace std;
using namespace HDLParser;
using namespace HDL;

vector<string> HDLToken::symbols = { "+", "-", ",", "..", "(", ")", "{", "}", "[", "]", ":", ";", "|", "=" };
vector<string> HDLToken::keywords = { "IMPORT", "CHIP", "TEST", "IN", "OUT", "PIN", "CLOCK", };
HDLToken::HDLToken(string value, int line_number = -1) : value(value), line_number(line_number) {
	if (isKeyword(value)) {
		type = Types::keyword;
	}
	else if (isSymbol(value)) {
		type = Types::symbol;
	}
	else if (isConstant(value)) {
		type = Types::constant;
	}
	else if (isIdentifier(value)) {
		type = Types::identifier;
	}
	else if (value == "eof") {
		type = (Types) -1;
	}
	else if (value.find(".hdl") != string::npos) {
		type = Types::dir;
	}
	else {
		cout << "ya screwed some shit up messed up an identifier or something" << endl;
		system("pause");
		system("exit");
	}
}
bool HDLToken::isKeyword(string value) {
	for (string keyword : keywords) {
		if (keyword == value) {
			return true;
		}
	}
	return false;
}
bool HDLToken::isSymbol(string value) {
	for (string symbol : symbols) {
		if (symbol == value) {
			return true;
		}
	}
	return false;
}
bool HDLToken::isConstant(string value) {
	string::const_iterator it = value.begin();
	while (it != value.end() && isdigit(*it)) {
		++it;
	}
	return it == value.end();
}
bool HDLToken::isIdentifier(string value) {
	if (!isKeyword(value)) {
		string::const_iterator it = value.begin();
		if (isalpha(*it)) {
			while (it != value.end() && isalnum(*it)) {
				++it;
			}
			return it == value.end();
		}
	}
	return false;
}
HDLToken HDLToken::nextToken(string& stream, int line_number = -1) {
	stream.erase(0, min(stream.find_first_not_of("\t"), stream.size()));  // get rid of leading tabs
	stream.erase(0, min(stream.find_first_not_of(" "), stream.size()));  // get rid of leading spaces
	if (stream.length() == 0) {
		return HDLToken("eof", line_number);
	}
	int minimum = stream.find(" ");
	if (minimum == string::npos) {
		minimum = (int) stream.length();
	}
	string symbol_found = "";
	for (string symbol : symbols) {
		int new_index = stream.find(symbol);
		if (new_index != string::npos && new_index < minimum) {
			minimum = new_index;
			symbol_found = symbol;
		}
	}
	if (minimum == 0) {
		minimum += symbol_found.length();
	}
	if (minimum < 0) {
		cout << "what the heck why is minimum in nextToken less than zero youre dumb" << endl;
		system("pause");
		system("exit");
	}
	HDLToken token = HDLToken(stream.substr(0, minimum), line_number);
	stream.erase(0, minimum);
	return token;
}

unordered_map<string, Subtypes> ASTNode::subtypemap = { { "IMPORT", Subtypes::IMPORT }, { "CHIP", Subtypes::CHIP }, { "TEST", Subtypes::TEST }, { "IN", Subtypes::IN }, { "OUT", Subtypes::OUT }, { "PIN", Subtypes::PIN }, { "CLOCK", Subtypes::CLOCK },
													 { "+", Subtypes::plus }, { "-", Subtypes::dash }, { ",", Subtypes::comma }, { "..", Subtypes::dotdot }, 
													 { "(", Subtypes::open_p }, { ")", Subtypes::close_p }, { "[", Subtypes::open_b }, { "]", Subtypes::close_b }, { "{", Subtypes::open_c }, { "}", Subtypes::close_c }, 
													 { ":", Subtypes::colon }, { ";", Subtypes::semicolon }, { "|", Subtypes::vertical }, { "=", Subtypes::equals } };
//ASTNode::ASTNode() {
//	token = HDLToken("eof");
//	subtype = (Subtypes)-1;
//}
ASTNode::ASTNode(Subtypes subtype) : subtype(subtype) {}
ASTNode::ASTNode(HDLToken token, Subtypes subtype) : token(token) {
	if (token.type == Types::constant || token.type == Types::identifier) {
		this->subtype = subtype;
	}
	else if (token.type == Types::keyword || token.type == Types::symbol) {
		this->subtype = subtypemap.at(token.value);
		if (this->subtype != subtype) {
			cout << "this token is simply not right" << endl << "expected " << (int)subtype << " indexed subtype but got " << token.value << endl;
			system("pause");
			system("exit");
		}
	}
} // should essentially be private and only used by getRoot()
ASTNode* ASTNode::getRoot(vector<HDLToken> tokens, Subtypes parent_node_type = Subtypes::start) {
	ASTNode* return_node = NULL;
	if (parent_node_type == Subtypes::start) {
		return_node = new ASTNode(tokens[0], Subtypes::file);
		while (tokens[0].value != "CHIP" && tokens.size() > 0) {  // get all import statements in separate branches
			if (tokens[0].value == "IMPORT") {
				for (int i = 1; i < tokens.size(); ++i) {
					if (tokens[i].type == Types::keyword) {
						return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::file));
						tokens.erase(tokens.begin(), tokens.begin() + i);
						break;
					}
				}
			}
		}
		if (tokens[0].value != "CHIP") {  // check that there is a CHIP statement
			cout << "where is 'CHIP' ya fuckin idiot what are you doing" << endl;
			system("pause");
			system("exit");
		}
		for (int i = 0; i < tokens.size(); ++i) {  // get the position of the TEST statement if there is one and get CHIP branch
			if (tokens[i].value == "TEST") {
				return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::file));
				tokens.erase(tokens.begin(), tokens.begin() + i);
				break;
			}
		}
		if (tokens.size() > 0) {  // get either CHIP or TEST branch, depending on if there was a TEST branch or not
			return_node->children.push_back(getRoot(tokens, Subtypes::file));
		}
	}
	if (parent_node_type == Subtypes::file) {
		if (tokens[0].value == "IMPORT") {  // IMPORT structure: (IMPORT) --> (fileDir) --> (semicolon) /// removed semicolons
			return_node = new ASTNode(tokens[0], Subtypes::importDec);
			tokens.erase(tokens.begin());
			return_node->children.push_back(getRoot(tokens, Subtypes::importDec));
		}
		else if (tokens[0].value == "CHIP") {  // CHIP structure: (CHIP) --> (chipName), ({), (bunch of decs-->), (})
			return_node = new ASTNode(tokens[0], Subtypes::chipDec);
			tokens.erase(tokens.begin());
			return_node->children.push_back(new ASTNode(tokens[0], Subtypes::chipName));
			tokens.erase(tokens.begin());
			return_node->children.push_back(new ASTNode(tokens[0], Subtypes::open_c));
			tokens.erase(tokens.begin());
			Types last_type = Types::keyword;
			for (int i = 0; i < tokens.size(); ++i) {  // finding the header and adding to the children
				if (tokens[i].type == Types::identifier && last_type == Types::identifier) {
					return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::chipDec));
					tokens.erase(tokens.begin(), tokens.begin() + i);
					break;
				}
				last_type = tokens[i].type;
			}
			for (int i = 0; i < tokens.size(); ++i) {  // getting the rest of the chip, i.e. the body, and adding a root node for the structure to the children
				if (tokens[i].value == "}") {
					return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::chipDec));
					tokens.erase(tokens.begin(), tokens.begin() + i);
					break;
				}
			}
			return_node->children.push_back(new ASTNode(tokens[0], Subtypes::close_c));  // adding final '}' to the children
		}
		else if (tokens[0].value == "TEST") {  // TEST structure  --- REDO once TEST definitions are clearer
			return_node = new ASTNode(tokens[0], Subtypes::testDec);
			return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin() + 1, tokens.end()), Subtypes::testDec));
		}
			//for (int k = 0; k < 2; ++k) {
			//	for (int i = 1; i < tokens.size(); ++i) {  // obviously tokens[0] should be either IN or OUT, which is a keyword
			//		if (tokens[i].type == Types::keyword) {  // first time is IN, second is OUT
			//			return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::chipDec));
			//			tokens.erase(tokens.begin(), tokens.begin() + i);
			//			break;
			//		}
			//	}
			//}
			//if (tokens[0].value != "PIN") {
			//	cout << "you forgot your pin declarations. you need to at least include 'PIN' in order to make a value program" << endl;
			//	system("pause");
			//	system("exit");
			//}
			//Types last_type = Types::keyword;
			//for (int i = 1; i < tokens.size(); ++i) {  // for PIN, special since CLOCK is optional
			//	if (tokens[i].type == Types::keyword || last_type == Types::identifier && tokens[i].type == Types::identifier) {
			//		return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::chipDec));
			//		tokens.erase(tokens.begin(), tokens.begin() + i);
			//		break;
			//	}
			//	last_type = tokens[i].type;
			//}
			//last_type = Types::keyword;
			//if (tokens[0].value == "CLOCK") {
			//	for (int i = 1; i < tokens.size(); ++i) {  // for CLOCK
			//		if (last_type == Types::identifier && tokens[i].type == Types::identifier) {
			//			return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::chipDec));
			//			tokens.erase(tokens.begin(), tokens.begin() + i);
			//			break;
			//		}
			//		last_type = tokens[i].type;
			//	}
			//}
		//	for (int i = 0; i < tokens.size(); ++i) {  // body structure
		//		if (tokens[i].value == "}") {
		//			return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::chipDec));
		//			tokens.erase(tokens.begin(), tokens.begin() + i);
		//		}
		//	}
		//	return_node->children.push_back(new ASTNode(tokens[0], Subtypes::close_c));
		//}
	}
	if (parent_node_type == Subtypes::importDec) {  /// jk about deprecated
		if (tokens.size() > 1) {
			cout << "unexpected token in IMPORT on line " << tokens[0].line_number << ".what were you thinking!?!?!?!?!?!?!?!" << endl;
			system("pause");
			system("exit");
		}
		return_node = new ASTNode(tokens[0], Subtypes::fileDir);
	}
	if (parent_node_type == Subtypes::chipDec) {
		if (tokens[0].type == Types::keyword) {  // is the header
			return_node = new ASTNode(Subtypes::header);
			for (int k = 0; k < 3; ++k) {  // for IN, OUT, and PIN. If CLOCK exists, it is checked for after
				for (int i = 1; i < tokens.size(); ++i) {
					if (tokens[i].type == Types::keyword) {
						return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i), Subtypes::header));
						tokens.erase(tokens.begin(), tokens.begin() + i);
						break;
					}
				}
			}
			if (tokens.size() > 0) {  // if CLOCK exists
				return_node->children.push_back(getRoot(tokens, Subtypes::header));
			}
		}
		else if (tokens[0].type == Types::identifier) {  // is the body
			return_node = new ASTNode(tokens[0], Subtypes::body);
			while (tokens.size() > 0) {
				for (int i = 0; i < tokens.size(); ++i) {
					if (tokens[i].value == ")") {
						return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i + 1), Subtypes::body));
						tokens.erase(tokens.begin(), tokens.begin() + i + 1);
					}
					else if (tokens[i].value == "(" && i > 1) {  // basically that '(' is on a new line or in a new partDec and I never came across a ')' to exit me from the for loop ebfore getting to it. usually '(' would be at i=1
						return_node->children.push_back(getRoot(vector<HDLToken>(tokens.begin(), tokens.begin() + i -1), Subtypes::body));  // - 1 because the '(' is in a different partDec
						tokens.erase(tokens.begin(), tokens.begin() + i - 1);  // same as above
					}
				}
			}
		}
		else {
			cout << "something is definitely wrong in your chip declaration" << endl;
			system("pause");
			system("exit");
		}
	}
	if (parent_node_type == Subtypes::testDec) {
		/// implement when the TEXT structure becomes clearer
	}
	if (parent_node_type == Subtypes::header) {
		if (tokens[0].type == Types::identifier) {
			return_node = new ASTNode(tokens[0], Subtypes::inName);
			tokens.erase(tokens.begin());
			if (tokens.size() > 0) {
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::open_b));
				tokens.erase(tokens.begin());
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::busSize));
				tokens.erase(tokens.begin());
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::close_b));
				tokens.erase(tokens.begin());
			}
		}
		else {
			cout << "where is your identifier? youre so stupid" << endl;
			system("pause");
			system("exit");
		}
	}
	if (parent_node_type == Subtypes::outDecs) {
		if (tokens[0].type == Types::identifier) {
			return_node = new ASTNode(tokens[0], Subtypes::inName);
			tokens.erase(tokens.begin());
			if (tokens.size() > 0) {
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::open_b));
				tokens.erase(tokens.begin());
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::busSize));
				tokens.erase(tokens.begin());
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::close_b));
				tokens.erase(tokens.begin());
			}
		}
		else {
			cout << "where is your identifier? youre so stupid" << endl;
			system("pause");
			system("exit");
		}
	}
	if (parent_node_type == Subtypes::pinDecs) {
		if (tokens[0].type == Types::identifier) {
			return_node = new ASTNode(tokens[0], Subtypes::inName);
			tokens.erase(tokens.begin());
			if (tokens.size() > 0) {
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::open_b));
				tokens.erase(tokens.begin());
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::busSize));
				tokens.erase(tokens.begin());
				return_node->children.push_back(new ASTNode(tokens[0], Subtypes::close_b));
				tokens.erase(tokens.begin());
			}
		}
		else {
			cout << "where is your identifier? youre so stupid" << endl;
			system("pause");
			system("exit");
		}
	}
	if (parent_node_type == Subtypes::clockDecs) {
		if (tokens[0].type == Types::identifier) {
			return_node = new ASTNode(tokens[0], Subtypes::inName);
		}
		else {
			cout << "where is your identifier? youre so stupid" << endl;
			system("pause");
			system("exit");
		}
	}
	if (parent_node_type == Subtypes::body) {
		if (tokens[1].value == "(") {  // this is a real, complex partDec
			return_node = new ASTNode(tokens[0], Subtypes::partDec);  // doesnt really matter what that first token is

		}
		else {  // this is just a pin = pin statement
			for (int i = 0; i < tokens.size(); ++i) {
				if (tokens[i].value == "=") {
					return_node = new ASTNode(tokens[i], Subtypes::partDec);
				}
			}
		}
	}
	if (parent_node_type == Subtypes::partDec) {

	}
	if (parent_node_type == Subtypes::in) {

	}
	if (parent_node_type == Subtypes::out) {

	}
	return return_node;
}

ASTNode* ASTNode::getRoot(vector<HDLToken> tokens, Subtypes parent_node_types = Subtypes::start) {

}