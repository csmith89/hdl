#pragma once

//#include "LexParse.h"


// TODO List
// make sure that all vectors deallocate properly if they are of pointers. This is important
// implement test
// make sure that circuit loops are handled in a way that either throws an error or forces some sort of time dependancy, preferably the former
// create a list of errors
// add in a line number for each token that it can print with the error



 /*Error list
 Unrecognized Token Error - should tell you the line number
 Invalid Syntax Error - should tell you the tokens related to the syntax error / line number
 Unclocked Loop Error - should tell you the name of the looping gates somehow, or the line number
 Mismatched Base Error - if youre in binary but you accidentailly use a 2, 3, 4, 5, 6, 7, 8, or 9
 Mismatched Range Error - should tell you the name of the gate and buses being connected
 Range Out Of Bounds Error - should tell you the name of the gate and bus being accessed/set
 Input Not Handled Error - is this really an error? or should it just default to all zeroes? prolly not an error
 Output Not Hangled Error - also not an error
 Input Multiply Defined - if you accidentially set the same input twice in one gate. Should tell you the line number or gate name and bus name
 Output Multiply Defined - just normal stuff two buses can have the same source
 Multiply defined identifier - if you import two chips with the same name

 Test Failed, but not really an error more a part of the functionality of the program*/